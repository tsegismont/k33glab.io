# K33g's website

## Test the website

- first, global installation: `npm install -g vuepress`
- you don't need to use `vuepress build`, the build is done by GitLab CI (see the `.gitlab-ci.yml`)
- to test/check your website: `vuepress dev docs`, then go to http://localhost:8080



## Resources:

- [Quick and easy VuePress website thanks to GitLab Pages](https://medium.com/@k33g_org/quick-and-easy-vuepress-website-thanks-to-gitlab-pages-6e02e631d1e)
