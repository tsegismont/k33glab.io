---
title: 🇬🇧 OpenFaas is also a PaaS - Part 1
lang: uk-UK
date: "2019-10-08"
month: "2019-10"
classification: "faas, gitlab, openfaas"
teaser: ""
---

# 🇬🇧 OpenFaas is also a PaaS - Part 1

And it's amazing. But, what does it mean in reality?
I wasn't aware of that (because I didn't read all the documentation *(1)*, and I'm so sorry for that, Alex). But you can deploy your micro services or web applications on **OpenFaaS** like on any other (good) PaaS.
If I developed a micro service, and I'm able to deploy it on **Clever Cloud** *(2)* or Heroku or any PaaS 12 factors compliant without any change *(3)*.

> - 1: OpenFaaS blog post reference: [https://www.openfaas.com/blog/stateless-microservices/](https://www.openfaas.com/blog/stateless-microservices/)
> - 2: try it, you will love it [https://www.clever-cloud.com/en/](https://www.clever-cloud.com/en/)
> - 3: except deployment files configuration

But, let's see how to do that. The easiest way is to Dockerize your application.

## First: create an ExpressJS web application

> The Part 2 (next blog post) will be about how to deploy **Vert-x** micro service from **GitLab CI** to **OpenFaaS**.

First, create a `hello-world` directory with 2 files:

- `index.js`
- `package.json`

> **`index.js`**
```javascript
const express = require('express')

const app = express()
const port = process.env.PORT || 8080

app.use(express.json())

app.get('/', (req, res, next) => {
  res.send("<h1>👋 Hello World 🌍</h1>")
})

app.get('/spock', (req, res, next) => {
  res.send({
    message:"🖖Live long and prosper!"
  })
})

app.listen(port, () => console.log(`🌍 listening on port ${port}`))
```

> Your web application must listen on the `8080` http port

> **`package.json`**
```json
{
  "name": "hello-world",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "start": "node ./index.js"
  },
  "license": "MIT",
  "dependencies": {
    "express": "^4.16.4"
  }
}
```

## Then, we will prepare our service for deployment on OpenFaaS

For that you need 2 files:

- `Dockerfile` *we are going to "dockerize" the web application*
- `config.deploy.yml` *the file needed by the OpenFaaS CLI to deploy the web application (and you can name it with another name if you want)*

> **`Dockerfile`**
```Dockerfile
FROM node:12.7.0-alpine

COPY . .
RUN npm install

CMD ["npm", "start"]
```

> - **Best practice**: your service should write a lock file to `/tmp/.lock`. It allows to write health check for OpenFaaS. You can do that with the `Dockerfile` or in you source code int the http start-up.
> - for example: `RUN touch /tmp/.lock`
> - better explained here: [https://docs.openfaas.com/reference/workloads/#stateless-microservices](https://docs.openfaas.com/reference/workloads/#stateless-microservices)

> **`config.deploy.yml`**
```yaml
version: 1.0
provider:
  name: openfaas
  gateway: ${OPENFAAS_URL}
functions:
  hello-world:
    lang: dockerfile
    handler: ./
    image: ${DOCKER_HANDLE}/hello-world:latest
```

> `lang: dockerfile` we'll use de Docker template

## Now, it's time to deploy

It's pretty simple, here are the commands to deploy:

```shell
export OPENFAAS_URL="http://openfaas.test:8080" # this is my OpenFaas instance
export OPENFASS_TOKEN="put here the OpenFaaS token"
export DOCKER_PASSWORD="your Docker hub password"
export DOCKER_HANDLE="your Docker hub handle"

echo -n ${DOCKER_PASSWORD} | docker login --username ${DOCKER_HANDLE} --password-stdin
echo -n ${OPENFASS_TOKEN} |faas-cli login --username=admin --password-stdin

faas-cli build -f config.deploy.yml
faas-cli push -f config.deploy.yml
faas-cli deploy -f config.deploy.yml

# or you can do it with only one command:
# faas-cli up -f config.deploy.yml
```

And now you can access to:

- your "web page": [http://openfaas.test:8080/function/hello-world](http://openfaas.test:8080/function/hello-world)
- your service: [http://openfaas.test:8080/function/hello-world/spock](http://openfaas.test:8080/function/hello-world/spock)


😃 Now you have your own PaaS. See you soon for the next part.

<disqus/>

<last-articles/>