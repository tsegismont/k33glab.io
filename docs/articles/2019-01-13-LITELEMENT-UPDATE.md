---
title: 🇬🇧 LitElement - Update with 2.0.0-rc.2 version
lang: us-US
date: "2019-01-13"
month: "2019-01"
classification: "javascript, litelement"
teaser: ""
---

# 🇬🇧 LitElement - Update with 2.0.0-rc.2 version

> Previously (read it before 🙏)
> - [How to start with LitElement](2019-01-08-LITELEMENT-01.html)
> - [Create components](2019-01-09-LITELEMENT-02.html)
> - [Talk to components](2019-01-12-LITELEMENT-03.html)

Friday, we can read on the **Polymer Blog** that a release candidate (`2.0.0-rc.2`) of **LitElement** was available [https://www.polymer-project.org/blog/2019-01-11-lit-element-rc](https://www.polymer-project.org/blog/2019-01-11-lit-element-rc).
So this morning I updated my previous posts (as well as the sample repository - all the branches)

The updates are simple:

First, I remove the `node_modules` directory, and then I update the `package.json` file like that:

```json
{
  "dependencies": {
    "@webcomponents/webcomponentsjs": "^2.2.3",
    "lit-element": "^2.0.0-rc.2"
  }
}
```
After that, I type the `yarn` command at the root of the project to fetch all dependencies.

Finally, for each component, change this line:

```javascript
import {LitElement, html} from '@polymer/lit-element'
```

by this:

```javascript
import {LitElement, html} from 'lit-element'
```

That's all. Now I'm ready to write a new blog post about LitElement.

<disqus/>

<last-articles/>