---
title: 🇬🇧 Add Disqus support to VuePress pages
lang: us-EN
date: "2018-07-02"
month: "2018-07"
classification: "JavaScript - VuePress"
teaser: ""
---

# 🇬🇧 Add Disqus support to VuePress pages

> 2018-07-02

There is a compelling feature with **VuePress** (*btw, this blog is propulsed by VuePress*), you can add **Vue Components** to you markdown pages.

I use **VuePress** as a blog, so I need that readers could comment on my articles. The most comfortable external service to do that is **[Disqus](https://disqus.com/)**.

So, once registered, you need to add a website (your website) in the administration panel of **Disqus** and you'll get a handle.

Then, add a new file (`disqus.vue`) in this directory: `docs/.vuepress/components` with this content:

```javascript
<template>
  <div id="disqus_thread"></div>
</template>
<script>
  export default {
    mounted() {
      var disqus_config = function () {
        this.page.url = window.location.origin;  
        this.page.identifier = window.location.pathname; 
      };
      (function() {
        var d = window.document, s = d.createElement('script');
        s.src = 'https://YOURHANDLE.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
      })();
    }
  }
</script>
```

:warning: :wave: **you need to replace `YOURHANDLE` by your handle of course**.

And now, each time you need comments on a page, add `<disqus/>` at the bottom of your markdown document.

That's all folks, so don't hesitate to comment :smile:

<disqus/>

<last-articles/>
